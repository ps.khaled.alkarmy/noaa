package com.progressoft;

import com.progressoft.repository.StationDAO;
import com.progressoft.repository.StationDAOFile;
import com.progressoft.util.Config;
import com.progressoft.util.Reader;
import com.progressoft.util.StationMapper;
import com.progressoft.util.StationMapperImp;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.text.ParseException;

public class MainTest {
    private static final StationMapper stationMapper = new StationMapperImp();
    private static final StationDAO StationDAOImp = new StationDAOFile();
    private static final Config config = new Config();
    private static final Reader reader = new Reader();


    @BeforeAll
    public static void init() {
        config.setDataSource("Stations.txt");
    }

    @Test
    public void givenValidFile_whenConstructed_then() throws ParseException {
        for (String line : reader.readAllLine(config.getDataSource("datasource.path"))) {
            StationDAOImp.saveStation(stationMapper.convert(line));
        }

        Assertions.assertThat(StationDAOImp.getByCountryCode("AF").size()).isGreaterThan(0);
        Assertions.assertThat(StationDAOImp.getByCountryCode("AF").get(0).getCTRY()).isEqualTo("AF");
        Assertions.assertThat(StationDAOImp.getByStationsIDRange("0", "999999").size()).isGreaterThan(0);
        Assertions.assertThat(StationDAOImp.getByGeographicalLocation("+71.100", "+023.983").size()).isGreaterThan(2);
        Assertions.assertThat(StationDAOImp.getStationByName("AQABA").size()).isGreaterThan(0);
        Assertions.assertThat(StationDAOImp.getStationByName("AQABA").get(1).toString()).isEqualTo("403410  |99999  |AQABA PORT                     |JO    |    |       |+29.483  |+034.983  |+0003.0  |19650806  |20031017 |");
    }

}
