package com.progressoft.initializer;

import com.progressoft.model.Station;
import com.progressoft.repository.StationDAO;
import com.progressoft.util.Reader;
import com.progressoft.util.StationMapper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class InitializerTest {

    @InjectMocks
    private Initializer initializer;

    @Mock
    private StationDAO stationDAO;

    @Mock
    private Reader reader;

    @Mock
    private StationMapper stationMapper;

    @Test
    void givenAnEmptyFile_whenInitialize_thenShouldNotInvokeMapperOrStationDao() throws Exception {
        when(reader.readAllLine(anyString())).thenReturn(new ArrayList<>());

        initializer.initialize("test file.txt");

        verifyNoInteractions(stationMapper);
        verifyNoInteractions(stationDAO);
    }

    @Test
    void givenANonEmptyFile_whenInitialize_thenShouldMapLineToStationAndSaveIt() throws Exception {
        when(reader.readAllLine(anyString())).thenReturn(Collections.singletonList("mock line"));

        Station testStation = new Station.Builder()
                .setStationName("Test Station")
                .build();

        when(stationMapper.convert(anyString())).thenReturn(testStation);

        initializer.initialize("test.txt");

        verify(stationDAO).saveStation(testStation);

        ArgumentCaptor<Station> stationArgumentCaptor = ArgumentCaptor.forClass(Station.class);
        verify(stationDAO).saveStation(stationArgumentCaptor.capture());

        Station savedStation = stationArgumentCaptor.getValue();
        Assertions.assertThat(savedStation).isEqualTo(testStation);
    }
}