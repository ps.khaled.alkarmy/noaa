package com.progressoft;

import com.progressoft.model.Station;
import com.progressoft.repository.StationDAO;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Collections;

import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class NoaaServiceTest {

    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @InjectMocks
    private NoaaService noaaService;

    @Mock
    private StationDAO stationDAO;

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }


    @Test
    void givenEmptyString_whenHandleInput_thenExpectedPrint() {
        noaaService.handleSearchOptions("");
        Assertions.assertThat("Invalid option  try again ^:)").isEqualTo(outputStreamCaptor.toString()
                .trim());
    }

    @Test
    void givenInvalidOption_whenHandleInput_thenExpectedPrint() {
        noaaService.handleSearchOptions("6");
        Assertions.assertThat("Invalid option 6 try again ^:)").isEqualTo(outputStreamCaptor.toString()
                .trim());
    }

    @Test
    void givenInvalidName_whenGetStationByName_thenShouldNotInvokeStationDAO() {
        noaaService.getStationByName("  ");
        verifyNoInteractions(stationDAO);
    }

    @Test
    void givenValidName_whenGetStationByName_thenShouldInvokeStationDAOAndGetStationsByName() {
        Station test = new Station.Builder().setStationName("test").build();
        when(stationDAO.getStationByName(anyString())).thenReturn(Collections.singletonList(test));
        noaaService.getStationByName("test");
        verifyNoMoreInteractions(stationDAO);
    }

    @Test
    void givenInvalidName_whenGetByCountryCode_thenShouldNotInvokeStationDAO() {
        noaaService.getByCountryCode("  ");
        verifyNoInteractions(stationDAO);
    }

    @Test
    void givenValidName_whenGetByCountryCode_thenShouldInvokeStationDAOAndGetStationsByCountryCode() {
        Station test = new Station.Builder().setStationName("test").build();
        when(stationDAO.getByCountryCode(anyString())).thenReturn(Collections.singletonList(test));
        noaaService.getByCountryCode("test");
        verifyNoMoreInteractions(stationDAO);
    }

    @Test
    void givenInvalidRange_whenGetStationsIdRange_thenShouldNotInvokeStationDAO() {
        noaaService.getStationsIdRange("  ", null);
        noaaService.getStationsIdRange(null, "null");
        noaaService.getStationsIdRange("null", "null");
        noaaService.getStationsIdRange("90000", "null");
        verifyNoInteractions(stationDAO);
    }

    @Test
    void givenValidRange_whenGetByCountryCode_thenShouldInvokeStationDAOAndGetStationsByCountryCode() {
        Station test = new Station.Builder().setStationName("test").build();
        when(stationDAO.getByStationsIDRange(anyString(), anyString())).thenReturn(Collections.singletonList(test));
        noaaService.getStationsIdRange("99999", "999999");
        verifyNoMoreInteractions(stationDAO);
    }

    @Test
    void givenInvalidLONAndLAT_whenGetByGeographicalLocation_thenShouldNotInvokeStationDAO() {
        noaaService.getByGeographicalLocation("", null);
        noaaService.getByGeographicalLocation(null, null);
        noaaService.getByGeographicalLocation(null, "null");
        noaaService.getByGeographicalLocation("+90.5", "null");
        verifyNoInteractions(stationDAO);
    }


    @Test
    void givenValidLONAndLAT_whenGetByGeographicalLocation_thenShouldInvokeStationDAOAndGetStationsByGeographicalLocation() {
        Station station = new Station.Builder().setStationName("test").build();
        when(stationDAO.getByGeographicalLocation(anyString(), anyString())).thenReturn(Collections.singletonList(station));
        noaaService.getByGeographicalLocation("+29.612 ", "+035.018");
        verifyNoMoreInteractions(stationDAO);
    }

    @Test
    void givenInvalidAndValidRow_whenIsValidRow_thenExceptionIsThrown() {
        Assertions.assertThat(noaaService.isValidRow("-10")).isFalse();
        Assertions.assertThat(noaaService.isValidRow("10")).isTrue();
        Assertions.assertThat(noaaService.isValidRow("A")).isFalse();
    }


    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }

}