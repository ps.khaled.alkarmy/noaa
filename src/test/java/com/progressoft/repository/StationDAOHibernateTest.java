package com.progressoft.repository;

import com.progressoft.model.Station;
import com.progressoft.util.Config;
import com.progressoft.util.Reader;
import com.progressoft.util.StationMapper;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.ParseException;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class StationDAOHibernateTest {

    private static final String line = "403400 99999 AQABA KING HUSSEIN INTL       JO      OJAQ  +29.612 +035.018 +0053.3 19610701 20230217";

    @Mock
    private Reader reader;
    private final StationDAO StationDAOImp = new StationDAOHibernate();
    @Mock
    private StationMapper stationMapper;
    @Mock
    private Config config;

    @Test
    void saveStation() throws ParseException {
        when(config.getDataSource(anyString())).thenReturn("Stations.txt");
        verifyNoMoreInteractions(config);

        when(reader.readAllLine(anyString())).thenReturn(Collections.singletonList(line));
        verifyNoMoreInteractions(reader);

        when(stationMapper.convert(anyString())).thenReturn(getStationObject());
        for (String line : reader.readAllLine(config.getDataSource("datasource.path"))) {
            StationDAOImp.saveStation(stationMapper.convert(line));
        }
    }

    @Test
    void getStations() {
        Assertions.assertThat(StationDAOImp.getStations().size()).isGreaterThan(30000);
    }

    @Test
    void getStationByName() {
        List<Station> stations = StationDAOImp.getStationByName("AQABA");
        Assertions.assertThat(stations.size()).isGreaterThanOrEqualTo(2);
    }

    @Test
    void getByCountryCode() {
        List<Station> stations = StationDAOImp.getByCountryCode("US");
        Assertions.assertThat(stations.size()).isGreaterThanOrEqualTo(300);

    }

    @Test
    void getByStationsIDRange() {
        List<Station> stations = StationDAOImp.getByStationsIDRange("690260", "726505");
        Assertions.assertThat(stations.size()).isGreaterThanOrEqualTo(300);
    }

    @Test
    void getByGeographicalLocation() {
        List<Station> stations = StationDAOImp.getByGeographicalLocation("+29.612", "+035.018");
        Assertions.assertThat(stations.size()).isGreaterThanOrEqualTo(7);
    }

    private static Station getStationObject() {
        return new Station.Builder()
                .setBeginDate("19750916")
                .setEndDate("19960701")
                .setStationName("RIO VISTA COAST GUARD LIGHT S")
                .setST("")
                .setCTRY("US")
                .setLAT("+38.150")
                .setLON("-121.700")
                .setCALL("")
                .setUSAF("745165")
                .setWBAN("99999")
                .setELEV("").build();
    }
}