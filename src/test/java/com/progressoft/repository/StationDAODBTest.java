package com.progressoft.repository;

import com.progressoft.model.Station;
import com.progressoft.util.*;
import com.progressoft.util.database.connectionpool.ConnectionPool;
import com.progressoft.util.database.connectionpool.ConnectionPoolImpl;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class StationDAODBTest {

    private static final String line= "403400 99999 AQABA KING HUSSEIN INTL       JO      OJAQ  +29.612 +035.018 +0053.3 19610701 20230217";

    @Spy
    private StationMapper stationMapper;
    @Mock
    private Config config;
    @Mock
    private Reader reader;
    private static final String url = "jdbc:mysql://localhost:3306/noaa";
    private static final String user = "root";
    private static final String password = "root";
    private static StationDAO StationDAOImp;

    @BeforeEach
    public void init() throws SQLException {
        stationMapper = spy(StationMapperImp.class);
        config = mock(Config.class);
        when(config.getDataSource(anyString())).thenReturn("Stations.txt");
        config.getDataSource("Stations.txt");
        verify(config).getDataSource("Stations.txt");

        ConnectionPool pool = ConnectionPoolImpl.create(url, user, password);
        StationDAOImp = new StationDAODB(pool, stationMapper);

    }


    @Test
    public void givenValidFile_whenConstructed_thenShouldInvokeStationMapperAndReaderAndStationDAO() throws ParseException {
        when(reader.readAllLine(anyString())).thenReturn(Collections.singletonList(line));
        verifyNoMoreInteractions(reader);

        Station station = getStationObject();
        when(stationMapper.convert(line)).thenReturn(station);
        stationMapper.convert(line);
        verifyNoMoreInteractions(stationMapper);

        for (String line : reader.readAllLine(config.getDataSource("dataSource"))) {
            StationDAOImp.saveStation(stationMapper.convert(line));
        }
        Assertions.assertThat(StationDAOImp.getStations().size()).isGreaterThanOrEqualTo(1000);
    }

    @Test
    public void givenInvalidFile_whenConstructed_thenShouldNotInvokeStationMapper() throws ParseException {
        when(reader.readAllLine(anyString())).thenReturn(Collections.emptyList());
        reader.readAllLine("mock");
        verifyNoMoreInteractions(reader);

        for (String line : reader.readAllLine(config.getDataSource("dataSource"))) {
            StationDAOImp.saveStation(stationMapper.convert(line));
        }

        verifyNoInteractions(stationMapper);
    }

    @Test
    void whenGetAllStations_thenExpectedResult() {
        List<Station> stations = StationDAOImp.getStations();
        Assertions.assertThat(stations).isNotNull();
        Assertions.assertThat(stations.size()).isGreaterThanOrEqualTo(30035);
    }

    @Test
    void whenGetAllStationsByName_thenExpectedResult() {
        List<Station> stations = StationDAOImp.getStationByName("Aqaba");
        Assertions.assertThat(stations).isNotNull();
    }

    @Test
    void whenGetAllStationsByCountryCode_thenExpectedResult() {
        List<Station> stations = StationDAOImp.getByCountryCode("US");
        Assertions.assertThat(stations).isNotNull();
    }

    @Test
    void whenGetByStationsIDRange_thenExpectedResult() {
        List<Station> stations = StationDAOImp.getByStationsIDRange("1000", "99999");
        Assertions.assertThat(stations).isNotNull();
    }

    @Test
    void whenGetByGeographicalLocation_thenExpectedResult() {
        Set<Integer> prop = Set.of(1,2,3,4,5,6,7,8,9,10,11);
        List<Station> stations = StationDAOImp.getByGeographicalLocation("+29.612", "+035.018");
        Assertions.assertThat(stations).isNotNull();
        new StationPrinter(prop).customToString(stations);
    }

    private static Station getStationObject() {
        return new Station.Builder()
                .setBeginDate("19750916")
                .setEndDate("19960701")
                .setStationName("RIO VISTA COAST GUARD LIGHT S")
                .setST("")
                .setCTRY("US")
                .setLAT("+38.150")
                .setLON("-121.700")
                .setCALL("")
                .setUSAF("745165")
                .setWBAN("99999")
                .setELEV("").build();
    }
}