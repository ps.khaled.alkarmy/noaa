package com.progressoft.repository;

import com.progressoft.model.Station;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.text.ParseException;

class StationDAOFileTest {

    private static StationDAOFile stationDAOFile;

    @BeforeAll
    public static void init() {
        stationDAOFile = new StationDAOFile();
    }

    @Test
    public void givenNullStation_whenSaveStation_thenExceptionIsThrow() {
        Assertions.assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> stationDAOFile.saveStation(null))
                .withMessage("Station is null");
    }

    @Test
    public void givenStation_whenSaveStation_thenResultExpected() throws ParseException {
        Assertions.assertThat(stationDAOFile.getStations()).isNotNull();
        Station station = new Station.Builder()
                .setBeginDate("19750916")
                .setEndDate("19960701")
                .setStationName("RIO VISTA COAST GUARD LIGHT S")
                .setST("")
                .setCTRY("US")
                .setLAT("+38.150")
                .setLON("-121.700")
                .setCALL("")
                .setUSAF("745165")
                .setWBAN("99999")
                .setELEV("").build();
        stationDAOFile.saveStation(station);
        Assertions.assertThat(stationDAOFile.getStations().size()).isEqualTo(1);
        Assertions.assertThat(stationDAOFile.getStations().get(0).getEndDate().getClass()).isEqualTo(String.class);
        Assertions.assertThat(stationDAOFile.getStations().get(0).getUSAF()).isEqualTo("745165");
        Assertions.assertThat(stationDAOFile.getStations().get(0).getWBAN()).isEqualTo("99999");
        Assertions.assertThat(stationDAOFile.getStations().get(0).getStationName()).isEqualTo("RIO VISTA COAST GUARD LIGHT S");
        Assertions.assertThat(stationDAOFile.getStations().get(0).getCTRY()).isEqualTo("US");
        Assertions.assertThat(stationDAOFile.getStations().get(0).getST()).isEqualTo("");
        Assertions.assertThat(stationDAOFile.getStations().get(0).getCALL()).isEqualTo("");
        Assertions.assertThat(stationDAOFile.getStations().get(0).getLAT()).isEqualTo("+38.150");
        Assertions.assertThat(stationDAOFile.getStations().get(0).getLON()).isEqualTo("-121.700");
        Assertions.assertThat(stationDAOFile.getStations().get(0).getELEV()).isEqualTo("");
        Assertions.assertThat(stationDAOFile.getStations().get(0).getBeginDate()).isEqualTo("19750916");
        Assertions.assertThat(stationDAOFile.getStations().get(0).getEndDate()).isEqualTo("19960701");
    }
}