package com.progressoft.util;

import com.progressoft.repository.StationDAOFile;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.text.ParseException;

class ReaderTest {

    @Test
    public void givenValidFile_whenReadFile_thenExpectedResult() throws ParseException {
        StationDAOFile stationDAOFile = new StationDAOFile();
        Reader reader = new Reader();
        StationMapper stationMapper = new StationMapperImp();
        for (String line : reader.readAllLine("Stations.txt")) {
            stationDAOFile.saveStation(stationMapper.convert(line));
        }

        Assertions.assertThat(stationDAOFile.getStations()).isNotNull();
        Assertions.assertThat(stationDAOFile.getStations().size()).isGreaterThan(0);
    }

}