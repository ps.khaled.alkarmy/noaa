package com.progressoft.util.database;

import com.progressoft.util.Config;
import com.progressoft.util.database.connectionpool.ConnectionPool;
import com.progressoft.util.database.connectionpool.ConnectionPoolImpl;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ConnectionPoolTest {
    @Mock
    private Config config;
    private static String url;
    private static String user;
    private static String password;
    private static ConnectionPool pool;

    @BeforeEach
    void givenValidConnectionInfo_whenCreate_thenExpectedResult() throws SQLException {
        when(config.getDataSource("datasource.url")).thenReturn("jdbc:mysql://localhost:3306/noaa");
        when(config.getDataSource("datasource.user")).thenReturn("root");
        when(config.getDataSource("datasource.password")).thenReturn("root");
        url = config.getDataSource("datasource.url");
        user = config.getDataSource("datasource.user");
        password = config.getDataSource("datasource.password");
        pool = ConnectionPoolImpl.create(url, user, password);
    }

    @Test
    void givenInvalidConnectionPool_whenCreate_thenExceptionIsThrown() {
        Assertions.assertThat(pool.getSize()).isEqualTo(10);
        Assertions.assertThat(pool.getPassword()).isEqualTo(password);
        Assertions.assertThat(pool.getUser()).isEqualTo(user);
        Assertions.assertThat(pool.getUrl()).isEqualTo(url);
        Connection connection = pool.getConnection();
        Assertions.assertThat(connection).isInstanceOf(Connection.class);
        Assertions.assertThat(pool.releaseConnection(connection)).isTrue();
        List<Connection> connections = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            connections.add(pool.getConnection());
        }
        Assertions.assertThat(connections.size()).isEqualTo(11);
        Assertions.assertThat(pool.getSize()).isEqualTo(11);
        Assertions.assertThat(pool.releaseConnection(connections.get(connections.size() - 1))).isTrue();
        pool.shutdown();
        Assertions.assertThat(pool.getSize()).isEqualTo(0);
    }

    @Test
    public void givenConnectionPool_whenGetConnectionAndReleaseConnection_thenExpectedResult() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 100000000; i++) {
            Connection connection = pool.getConnection();
            pool.releaseConnection(connection);
        }
        long end = System.currentTimeMillis();
        System.out.println("Time spent: " + (end - start) + "ms");
    }
}