package com.progressoft.util;

import com.progressoft.repository.StationDAO;
import com.progressoft.repository.StationDAOFile;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.text.ParseException;


class StationMapperTest {
    @Test
    void givenValidDAOAndValidList_whenConvert_thenExpectedResult() throws ParseException {
        StationMapper stationMapper = new StationMapperImp();
        StationDAO stationDAO = new StationDAOFile();
        String list = "998405 99999 MANISTEE HARBOR               US   MI       +44.248 -086.346 +0179.5 20091114 20170924";
        stationDAO.saveStation(stationMapper.convert(list));
        Assertions.assertThat(stationDAO.getStations()).isNotNull();
        Assertions.assertThat(stationDAO.getStations().size()).isEqualTo(1);
        Assertions.assertThat(stationDAO.getStations().get(0).getStationName()).isEqualTo("MANISTEE HARBOR");
        Assertions.assertThat(stationDAO.getStations().get(0).getUSAF()).isEqualTo("998405");
    }
}