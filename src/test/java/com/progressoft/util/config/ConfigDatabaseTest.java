package com.progressoft.util.config;

import com.progressoft.exceptions.StationException;
import com.progressoft.util.Config;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


class ConfigDatabaseTest {

    static String url = "jdbc:mysql://localhost:3306/noaa";
    static String user = "root";
    static String password = "root";
    private static Config config;

    private static Set<Integer> rows;

    @AfterAll
    public static void finish() {
        config.setDataSource(url, user, password);
    }

    @BeforeEach
    public void init() {
        config = new Config();
        config.setDataSource(url, user, password);
        rows = new HashSet<>();
    }

    @Test
    public void givenInvalidRow_whenSetProperties_thenExceptionIsThrown() {
        rows.add(15);
        rows.add(13);
        rows.add(16);
        Assertions.assertThatExceptionOfType(StationException.class).isThrownBy(() -> config.setRows(12)).withMessage("Invalid Row");
        Assertions.assertThatExceptionOfType(StationException.class).isThrownBy(() -> config.setRows(rows)).withMessage("Invalid Row");
        Assertions.assertThatCode(() -> config.setRows(11)).doesNotThrowAnyException();
    }

    @Test
    public void givenValidRow_whenSetProperties_thenExpectedResult() {
        rows.add(1);
        rows.add(2);
        rows.add(3);
        Assertions.assertThatCode(() -> config.setRows(rows)).doesNotThrowAnyException();
        List<Integer> properties = config.getRows();
        Assertions.assertThat(rows.containsAll(properties)).isTrue();
        Assertions.assertThat(config.getDataSource("datasource.url")).isEqualTo("jdbc:mysql://localhost:3306/noaa");
        Assertions.assertThat(config.getDataSource("datasource.user")).isEqualTo("root");
        Assertions.assertThat(config.getDataSource("datasource.password")).isEqualTo("root");
    }

}
