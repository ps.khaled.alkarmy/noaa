package com.progressoft.util;

import com.progressoft.model.Station;
import com.progressoft.repository.StationDAO;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.text.ParseException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


class StationPrinterTest {

    private static final Set<Integer> row = new HashSet<>();
    @Mock
    private static StationDAO stationDAO;
    private static StationPrinter printer;

    @BeforeAll
    public static void init() {
        stationDAO = mock(StationDAO.class);
        Station station = new Station.Builder()
                .setUSAF("998405")
                .setWBAN("99999")
                .setStationName("MANISTEE HARBOR")
                .setCTRY("US")
                .setST("")
                .setCALL("MI")
                .setLAT("+44.248")
                .setLON("-086.346 ")
                .setELEV("+0179.5")
                .setBeginDate("20091114")
                .setEndDate("20170924")
                .build();
        when(stationDAO.getStations()).thenReturn(List.of(station));
        for (int i = 1; i <= 11; i++) {
            row.add(i);
        }
        printer = new StationPrinter(row);


    }

    @Test
    public void givenPrinter_whenPrintHeader_thenExpectedResult() {
        String expectedHeader = "USAF    | WBAN   | STATION NAME                   | CTRY  | ST  | CALL   | LAT      | LON       | ELEV(M)  | BEGIN     | END      | ";
        Assertions.assertThat(printer.buildHeader()).isEqualTo(expectedHeader);
    }

    @Test
    public void givenPrinter_whenPrintStationAsList_thenExpectedResult() throws ParseException {
        String expected = "998405 | 99999 | MANISTEE HARBOR | US |  | MI | +44.248 | -086.346  | +0179.5 | 20091114 | 20170924 | ";
        Station station = stationDAO.getStations().get(0);
        Assertions.assertThat(printer.print(station)).isEqualTo(expected);
    }
}