package com.progressoft;

import com.progressoft.exceptions.ExceptionHandler;
import com.progressoft.exceptions.StationException;
import com.progressoft.model.Station;
import com.progressoft.repository.StationDAO;
import com.progressoft.util.Config;
import com.progressoft.util.StationPrinter;

import java.util.*;

public class NoaaService {

    private static final String mainOptions = "Select row(s) you want to display: \n" +
            "[1] All (default)\n" +
            "[2] Custom row";
    private static final String options = "\nEnter your selection:\n" +
            "\n" +
            "1 - Search for station by name\n" +
            "\n" +
            "2- Search stations by country code\n" +
            "\n" +
            "3- Search stations by stations ID range\n" +
            "\n" +
            "4- Search stations by Geographical location.\n" +
            "\n" +
            "5- Exit ";

    private static final String rowsOptions = "Select Row  by number:\n" +
            "[1] USAF: Air Force station ID.\n" +
            "[2] WBAN: NCDC WBAN number.\n" +
            "[3] STATION NAME\n" +
            "[4] CTRY: FIPS country ID.\n" +
            "[5] ST: State for US stations.\n" +
            "[6] CALL\n" +
            "[7] LAT\n" +
            "[8] LON\n" +
            "[9] ELEV(M)\n" +
            "[10] BEGIN\n" +
            "[11] END\n" +
            "press \"y\" for if finished ";
    private static final Scanner scanner = new Scanner(System.in);
    private final StationDAO stationDAOImp;
    private final Config config;
    private StationPrinter printer;

    public NoaaService(StationDAO stationDAOImp, Config config) {
        this.stationDAOImp = stationDAOImp;
        this.config = config;
    }

    public void run() {
        Set<Integer> rowsSet = new HashSet<>();
        System.out.println(mainOptions);
        String option = scanner.nextLine();

        if (option.equals("1")) {
            fillProsperityWithDefaultRows(rowsSet);
        } else if (option.equals("2")) {
            System.out.println(rowsOptions);
            handelRowOptions(rowsSet);
        }
        config.setRows(rowsSet);

        printer = new StationPrinter(rowsSet);

        while (true) {
            System.out.println(options);
            option = scanner.nextLine();
            handleSearchOptions(option);
        }
    }

    private void handelRowOptions(Set<Integer> rowsSet) {
        String option;
        while (true) {
            option = scanner.nextLine();
            if (option.equals("y")) {
                break;
            }
            if (isValidRow(option))
                rowsSet.add(Integer.parseInt(option));
        }
    }

    private void fillProsperityWithDefaultRows(Set<Integer> rowsSet) {
        for (int i = 1; i <= 11; i++) {
            rowsSet.add(i);
        }
    }

    public boolean isValidRow(String row) {
        try {
            int r = Integer.parseInt(row);
            if (r <= 0 || r > 11) {
                System.out.println("Invalid row number");
                return false;
            }
        } catch (Exception ignored) {
            System.out.println("Character is not allowed");
            return false;
        }
        return true;
    }

    public void handleSearchOptions(String option) {
        if (!validInputOption(option)) {
            System.out.printf("Invalid option %s try again ^:)\n", option);
            return;
        }
        switch (option) {
            case "1":
                System.out.println("Enter station name: ");
                String name = scanner.nextLine();
                printResult(getStationByName(name));
                break;
            case "2":
                System.out.println("Enter Country code: ");
                String code = scanner.nextLine();
                printResult(getByCountryCode(code));
                break;
            case "3":
                System.out.println("Enter min range : ");
                String minRange = scanner.nextLine();
                System.out.println("Enter max range : ");
                String maxRange = scanner.nextLine();
                printResult(getStationsIdRange(minRange, maxRange));
                break;
            case "4":
                System.out.println("Enter latitude : ");
                String latitude = scanner.nextLine();
                System.out.println("Enter longitude : ");
                String longitude = scanner.nextLine();
                printResult(getByGeographicalLocation(latitude, longitude));
                break;
            case "5":
                System.exit(0);
        }
    }

    public List<Station> getStationByName(String name) {
        if (!isValidName(name)) {
            ExceptionHandler.handleExceptions(new StationException("Invalid name"));
            return Collections.emptyList();
        }
        return stationDAOImp.getStationByName(name);
    }


    public List<Station> getByCountryCode(String code) {
        if (!isValidName(code)) {
            ExceptionHandler.handleExceptions(new StationException("Invalid code"));
            return Collections.emptyList();
        }
        return stationDAOImp.getByCountryCode(code.replace("\n", ""));
    }

    public List<Station> getStationsIdRange(String minRange, String maxRange) {
        if (!isValidRange(minRange, maxRange)) {
            ExceptionHandler.handleExceptions(new StationException("Invalid range"));
            return Collections.emptyList();
        }
        return stationDAOImp.getByStationsIDRange(minRange, maxRange);

    }

    public List<Station> getByGeographicalLocation(String latitude, String longitude) {
        if (!isValidLatLon(latitude, longitude)) {
            ExceptionHandler.handleExceptions(new StationException("Invalid Latitude and Longitude Pairs"));
            return Collections.emptyList();
        }
        return stationDAOImp.getByGeographicalLocation(latitude, longitude);
    }

    private boolean isValidLatLon(String latitude, String longitude) {
        if (Objects.isNull(latitude) || Objects.isNull(longitude)) return false;
        double lat;
        double lon;
        try {
            lat = Double.parseDouble(latitude);
            lon = Double.parseDouble(longitude);
        } catch (NumberFormatException exception) {
            ExceptionHandler.handleExceptions(new StationException("Invalid Latitude and Longitude Pairs"));
            return false;
        }

        if (lat < -90 || lat > 90)
            return false;
        return !(lon < -180) && !(lon > 180);
    }

    private boolean isValidRange(String minRange, String maxRange) {
        if (Objects.isNull(maxRange) || Objects.isNull(minRange)) return false;
        int min, max;
        try {
            min = parseUsaf(minRange);
            max = parseUsaf(maxRange);
        } catch (NumberFormatException exception) {
            ExceptionHandler.handleExceptions(new StationException("Invalid range"));
            return false;
        }
        if (min < 0 || max < 0) return false;
        return min <= max;
    }

    private int parseUsaf(String usaf) {
        if (usaf.matches("^[A-Za-z]\\d+$")) {
            usaf = usaf.substring(1);
            return Integer.parseInt(usaf) + 'A' * 1000000;
        }
        return Integer.parseInt(usaf);
    }

    private boolean validInputOption(String option) {
        if (option.isEmpty()) return false;
        int intOption = Integer.parseInt(option);
        return intOption <= 5;
    }

    private boolean isValidName(String name) {
        return !(name.isEmpty() || name.trim().isBlank());
    }

    private void printResult(List<Station> stations) {
        printer.customToString(stations);
        System.out.println("\u001B[1;96m" + "Total result: " + stations.size() + "\u001B[0m");
    }

}
