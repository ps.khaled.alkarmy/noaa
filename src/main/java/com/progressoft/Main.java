package com.progressoft;

import com.progressoft.initializer.Initializer;
import com.progressoft.repository.StationDAO;
import com.progressoft.repository.StationDAODB;
import com.progressoft.util.Config;
import com.progressoft.util.Reader;
import com.progressoft.util.StationMapper;
import com.progressoft.util.StationMapperImp;
import com.progressoft.util.database.connectionpool.ConnectionPool;
import com.progressoft.util.database.connectionpool.ConnectionPoolImpl;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Scanner;

public class Main {

    public static final String DATASOURCE_OPTIONS = "Select Datasource: \n" +
            "[1] File\n" +
            "[2] Database";

    private static final StationMapper stationMapper = new StationMapperImp();
    private static final Config config = new Config();
    private static StationDAO stationDAOImp;

    public static void main(String[] args) throws ParseException, SQLException {
        Scanner scanner = new Scanner(System.in);
        System.out.println(DATASOURCE_OPTIONS);
        String datasourceOption = scanner.nextLine();
        stationDAOImp = handleDatasourceOptions(stationDAOImp, scanner, datasourceOption);
        new NoaaService(stationDAOImp, config).run();
    }

    private static StationDAO handleDatasourceOptions(StationDAO stationDAOImp, Scanner scanner, String datasourceOption) throws ParseException, SQLException {
        if (datasourceOption.equals("1")) {
            if (Main.config.getDataSource("datasource.path") == null) {
                System.out.println("Enter file name: ");
                String fileName = scanner.nextLine();
                Main.config.setDataSource(fileName);
            }
            new Initializer(stationDAOImp, new Reader(), Main.stationMapper).initialize(Main.config.getDataSource("datasource.path"));
        } else if (datasourceOption.equals("2")) {
            if (Main.config.getDataSource("datasource.url") == null) {
                System.out.println("Enter url : ");
                String url = scanner.nextLine();
                System.out.println("Enter user : ");
                String user = scanner.nextLine();
                System.out.println("Enter password : ");
                String password = scanner.nextLine();
                Main.config.setDataSource(url, user, password);
            }
            ConnectionPool connector = getPool();
            stationDAOImp = new StationDAODB(connector, Main.stationMapper);
            System.out.println("You are connected to database.\n" +
                    "Url: " + connector.getUrl() + "\n" +
                    "User: " + connector.getUser() + "\n"
            );
        }
        return stationDAOImp;
    }

    private static ConnectionPool getPool() throws SQLException {
        String url = Main.config.getDataSource("datasource.url");
        String user = Main.config.getDataSource("datasource.user");
        String password = Main.config.getDataSource("datasource.password");
        return ConnectionPoolImpl.create(url, user, password);
    }
}
