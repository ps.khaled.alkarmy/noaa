package com.progressoft.initializer;

import com.progressoft.repository.StationDAO;
import com.progressoft.util.Reader;
import com.progressoft.util.StationMapper;

import java.text.ParseException;

public class Initializer {

    private final StationDAO stationDAO;
    private final Reader reader;
    private final StationMapper stationMapper;

    public Initializer(StationDAO stationDAO, Reader reader, StationMapper stationMapper) {
        this.stationDAO = stationDAO;
        this.reader = reader;
        this.stationMapper = stationMapper;
    }

    public void initialize(String dataSource) throws ParseException {
        for (String line : this.reader.readAllLine(dataSource)) {
            this.stationDAO.saveStation(this.stationMapper.convert(line));
        }
    }
}
