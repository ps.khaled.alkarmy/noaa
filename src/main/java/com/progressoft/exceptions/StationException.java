package com.progressoft.exceptions;

public class StationException extends RuntimeException {
    public StationException(String message) {
        super(message);
    }
}
