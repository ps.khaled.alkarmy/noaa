package com.progressoft.exceptions;

public class ExceptionHandler {
    public static void handleExceptions(Exception e) {
        switch (e.getMessage()) {
            case "Invalid Latitude and Longitude Pairs":
                System.out.println("Invalid Latitude and Longitude Pairs");
                break;
            case "Invalid name":
                System.out.println("Invalid name");
                break;
            case "Invalid code":
                System.out.println("Invalid code");
                break;
            case "Invalid range":
                System.out.println("Invalid range");
                break;
        }
    }
}
