package com.progressoft.model;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "stations")
public class Station {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer Id;
    private String USAF;
    private String WBAN;
    @Column(name = "STATION_NAME")
    private String StationName;
    private String CTRY;
    private String ST;
    @Column(name = "\"CALL\"")
    private String CALL;
    private String LAT;
    private String LON;
    private String ELEV;
    @Column(name = "\"BEGIN\"")
    private String beginDate;
    @Column(name = "\"END\"")
    private String endDate;

    public Station(String USAF, String WBAN, String stationName, String CTRY, String ST, String CALL, String LAT, String LON, String ELEV, String beginDate, String endDate) {
        this.USAF = USAF;
        this.WBAN = WBAN;
        StationName = stationName;
        this.CTRY = CTRY;
        this.ST = ST;
        this.CALL = CALL;
        this.LAT = LAT;
        this.LON = LON;
        this.ELEV = ELEV;
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    private Station(Builder builder) {
        this.USAF = builder.USAF;
        this.WBAN = builder.WBAN;
        this.StationName = builder.StationName;
        this.CTRY = builder.CTRY;
        this.ST = builder.ST;
        this.CALL = builder.CALL;
        this.LAT = builder.LAT;
        this.LON = builder.LON;
        this.ELEV = builder.ELEV;
        this.beginDate = builder.beginDate;
        this.endDate = builder.endDate;
    }

    public Station() {

    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (String cell : this.asList()) {
            builder.append(cell).append(" |");
        }
        return builder.toString();
    }

    public String getUSAF() {
        return USAF.trim();
    }

    public String getWBAN() {
        return WBAN.trim();
    }

    public String getStationName() {
        return StationName.trim();
    }

    public String getCTRY() {
        return CTRY.trim();
    }

    public String getLAT() {
        return LAT.trim();
    }

    public String getLON() {
        return LON.trim();
    }

    public String getELEV() {
        return ELEV.trim();
    }

    public String getBeginDate() {
        return beginDate.trim();
    }

    public String getEndDate() {
        return endDate.trim();
    }

    public List<String> asList() {
        List<String> result = new ArrayList<>();
        result.add(USAF);
        result.add(WBAN);
        result.add(StationName);
        result.add(CTRY);
        result.add(ST);
        result.add(CALL);
        result.add(LAT);
        result.add(LON);
        result.add(ELEV);
        result.add(beginDate);
        result.add(endDate);
        return result;
    }

    public String getST() {
        return ST;
    }

    public String getCALL() {
        return CALL;
    }

    public static class Builder {
        private Integer Id;
        private String USAF;
        private String WBAN;
        private String StationName;
        private String CTRY;
        private String ST;
        private String CALL;
        private String LAT;
        private String LON;
        private String ELEV;
        private String beginDate;
        private String endDate;

        public Builder setId(Integer id) {
            Id = id;
            return this;
        }

        public Builder setUSAF(String USAF) {
            this.USAF = USAF;
            return this;
        }

        public Builder setWBAN(String WBAN) {
            this.WBAN = WBAN;
            return this;
        }

        public Builder setStationName(String stationName) {
            StationName = stationName;
            return this;
        }

        public Builder setCTRY(String CTRY) {
            this.CTRY = CTRY;
            return this;
        }

        public Builder setST(String ST) {
            this.ST = ST;
            return this;
        }

        public Builder setCALL(String CALL) {
            this.CALL = CALL;
            return this;
        }

        public Builder setLAT(String LAT) {
            this.LAT = LAT;
            return this;
        }

        public Builder setLON(String LON) {
            this.LON = LON;
            return this;
        }

        public Builder setELEV(String ELEV) {
            this.ELEV = ELEV;
            return this;
        }

        public Builder setBeginDate(String beginDate) {
            this.beginDate = beginDate;
            return this;
        }

        public Builder setEndDate(String endDate) {
            this.endDate = endDate;
            return this;
        }

        public Station build() {
            return new Station(this);
        }
    }
}
