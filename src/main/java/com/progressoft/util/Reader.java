package com.progressoft.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class Reader {
    public List<String> readAllLine(String path) {
        InputStream resourceAsStream = Reader.class.getClassLoader().getResourceAsStream(path);
        List<String> lines = new ArrayList<>();
        try {
            assert resourceAsStream != null;
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(resourceAsStream, StandardCharsets.UTF_8))) {
                for (String line; (line = reader.readLine()) != null; ) {
                    lines.add(line);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return lines;
    }
}
