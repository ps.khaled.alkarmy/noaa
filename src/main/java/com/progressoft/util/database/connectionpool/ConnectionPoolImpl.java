package com.progressoft.util.database.connectionpool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ConnectionPoolImpl implements ConnectionPool {
    private final String username;
    private final String password;
    private final String url;
    private final List<Connection> availableConnections;
    private final List<Connection> usedConnections = new ArrayList<>();

    private ConnectionPoolImpl(String url, String username, String password, List<Connection> avalabileConnections) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.availableConnections = avalabileConnections;
    }


    public static ConnectionPoolImpl create(String url, String user, String password) throws SQLException {
        failIfInvalid(url, user, password);
        List<Connection> connectionPool = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            connectionPool.add(createConnection(url, user, password));
        }
        return new ConnectionPoolImpl(url, user, password, connectionPool);
    }

    private static void failIfInvalid(String url, String user, String password) {
        if (Objects.isNull(url))
            throw new IllegalArgumentException("invalid url can't be null");
        if (Objects.isNull(user))
            throw new IllegalArgumentException("invalid user can't be null");
        if (Objects.isNull(password))
            throw new IllegalArgumentException("invalid password can't be null");
        if (url.trim().isEmpty())
            throw new IllegalArgumentException("invalid url can't be empty");
        if (user.trim().isEmpty())
            throw new IllegalArgumentException("invalid user can't be empty");

    }

    private static Connection createConnection(String url, String user, String password) {
        try {
            return DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Connection getConnection() {
        if (availableConnections.isEmpty()) {
            Connection connection = createConnection(url, username, password);
            availableConnections.add(connection);
        }

        Connection connection = availableConnections.remove(availableConnections.size() - 1);
        usedConnections.add(connection);
        return connection;
    }

    @Override
    public boolean releaseConnection(Connection connection) {
        availableConnections.add(connection);
        return usedConnections.remove(connection);
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public String getUser() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public int getSize() {
        return availableConnections.size() + usedConnections.size();
    }

    @Override
    public void shutdown() {
        for (Connection usedConnection : usedConnections) {
            try {
                usedConnection.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        for (Connection connection : availableConnections) {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        usedConnections.clear();
        availableConnections.clear();
    }
}
