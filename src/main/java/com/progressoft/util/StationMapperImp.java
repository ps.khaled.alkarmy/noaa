package com.progressoft.util;

import com.progressoft.model.Station;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StationMapperImp implements StationMapper {

    @Override
    public Station convert(String token) {
        String USAF = token.substring(0, 7);
        String WBAN = token.substring(7, 13);
        String StationName = token.substring(13, 43);
        String CTRY = token.substring(43, 48);
        String ST = token.substring(48, 51);
        String CALL = token.substring(51, 57);
        String LAT = token.substring(57, 65);
        String LON = token.substring(65, 74);
        String ELEV = token.substring(74, 82);
        String beginDate = token.substring(82, 91);
        String endDate = token.substring(91);
        return new Station(USAF, WBAN, StationName, CTRY, ST, CALL, LAT, LON, ELEV, beginDate, endDate);
    }


    @Override
    public Station convert(ResultSet resultSet) throws SQLException {
        String USAF = buildRow(resultSet.getString(2), 7);
        String WBAN = buildRow(resultSet.getString(3), 6);
        String StationName = buildRow(resultSet.getString(4), 30);
        String CTRY = buildRow(resultSet.getString(5), 5);
        String ST = buildRow(resultSet.getString(6), 3);
        String CALL = buildRow(resultSet.getString(7), 6);
        String LAT = buildRow(resultSet.getString(8), 8);
        String LON = buildRow(resultSet.getString(9), 9);
        String ELEV = buildRow(resultSet.getString(10), 8);
        String beginDate = buildRow(resultSet.getString(11), 9);
        String endDate = buildRow(resultSet.getString(12), 8);
        return new Station(USAF, WBAN, StationName, CTRY, ST, CALL, LAT, LON, ELEV, beginDate, endDate);
    }

    private String buildRow(String cell, int length) {
        int repeat = Math.abs(length - cell.length());
        return cell + " ".repeat(repeat);
    }

}
