package com.progressoft.util;

import com.progressoft.model.Station;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;


public interface StationMapper {
    Station convert(String line) throws ParseException;

    Station convert(ResultSet resultSet) throws SQLException;
}
