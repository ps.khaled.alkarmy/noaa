package com.progressoft.util;

import com.progressoft.exceptions.StationException;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

public class Config {
    private static final Properties properties = new Properties();
    private final OutputStream output;
    private final ArrayList<Integer> rows = new ArrayList<>();

    public Config() {
        try {
            InputStream input = Config.class.getClassLoader().getResourceAsStream("config.properties");
            output = new FileOutputStream("src/main/resources/config.properties");
            properties.load(input);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private boolean isValidRowSet(Integer row) {
        return row <= 11;
    }

    private boolean isValidRowSet(Set<Integer> rows) {
        for (Integer row : rows) {
            if (row <= 11)
                return true;
        }
        return false;
    }

    public List<Integer> getRows() {
        return rows;
    }

    public void setRows(Integer row) {
        if (!isValidRowSet(row)) {
            throw new StationException("Invalid Row");
        }
        rows.add(row);
        properties.setProperty("row", String.valueOf(rows));
        try {
            properties.store(output, null);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void setRows(Set<Integer> row) {
        if (!isValidRowSet(row)) {
            throw new StationException("Invalid Row");
        }
        rows.addAll(row);
        properties.setProperty("row", String.valueOf(rows));
        try {
            properties.store(output, null);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getDataSource(String property) {
        return (String) properties.get(property);
    }

    public void setDataSource(String dataSource) {
        properties.setProperty("datasource.path", dataSource);
    }

    public void setDataSource(String url, String user, String password) {
        properties.setProperty("datasource.url", url);
        properties.setProperty("datasource.user", user);
        properties.setProperty("datasource.password", password);
    }

}
