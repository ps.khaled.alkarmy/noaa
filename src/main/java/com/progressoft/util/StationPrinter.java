package com.progressoft.util;

import com.progressoft.model.Station;

import java.util.List;
import java.util.Set;

public class StationPrinter {
    public static final String headerRow = "USAF   WBAN  STATION NAME                  CTRY ST CALL  LAT     LON      ELEV(M) BEGIN    END     ";
    private final Set<Integer> prop;
    private final List<String> header = new StationMapperImp().convert(headerRow).asList();

    public StationPrinter(Set<Integer> prop) {
        this.prop = prop;
    }

    public void customToString(List<Station> stations) {
        System.out.println(buildHeader());
        for (Station station : stations) {
            System.out.println(print(station));
        }
    }

    String print(Station station) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < station.asList().size(); ) {
            if (prop.contains(i + 1)) {
                stringBuilder.append(station.asList().get(i)).append(" | ");
            }
            i++;
        }
        return stringBuilder.toString();
    }

    String buildHeader() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < header.size(); ) {
            if (prop.contains(i + 1)) {
                stringBuilder.append(header.get(i)).append(" | ");
            }
            i++;
        }
        return stringBuilder.toString();
    }
}



