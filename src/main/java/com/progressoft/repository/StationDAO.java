package com.progressoft.repository;

import com.progressoft.model.Station;

import java.util.List;

public interface StationDAO {
    void saveStation(Station station);

    List<Station> getStations();

    List<Station> getStationByName(String name);

    List<Station> getByCountryCode(String code);

    List<Station> getByStationsIDRange(String minRange, String maxRange);

    List<Station> getByGeographicalLocation(String LAT, String LON);

}
