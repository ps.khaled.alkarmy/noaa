package com.progressoft.repository;

import com.progressoft.model.Station;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class StationDAOFile implements StationDAO {
    private final List<Station> stations;

    public StationDAOFile() {
        stations = new ArrayList<>();
    }

    @Override
    public void saveStation(Station station) {
        if (Objects.isNull(station))
            throw new IllegalArgumentException("Station is null");
        stations.add(station);
    }

    @Override
    public List<Station> getStations() {
        return stations;
    }

    @Override
    public List<Station> getStationByName(String name) {
        String CapitalizeName = name.toUpperCase();
        return stations.stream().filter(s -> s.getStationName().contains(CapitalizeName)).collect(Collectors.toList());
    }

    @Override
    public List<Station> getByCountryCode(String code) {
        return stations.stream().filter(s -> s.getCTRY().equals(code)).collect(Collectors.toList());
    }

    @Override
    public List<Station> getByStationsIDRange(String minRange, String maxRange) {
        return stations.stream().filter(s -> {
            int range = 0;
            if (!s.getUSAF().equals("USAF")) {
                range = parseUsaf(s.getUSAF());
            }
            return range < parseUsaf(maxRange) && range > parseUsaf(minRange);
        }).collect(Collectors.toList());
    }

    //http://janmatuschek.de/LatitudeLongitudeBoundingCoordinates
    @Override
    public List<Station> getByGeographicalLocation(String LAT, String LON) {
        double lat1 = Math.toRadians(Double.parseDouble(LAT));
        double lon1 = Math.toRadians(Double.parseDouble(LON));
        List<Station> stationsList = new ArrayList<>();
        for (Station station : getStations()) {
            if (!station.getLAT().isEmpty() && !station.getLON().isEmpty() && !station.getLON().equals("LON") && !station.getLAT().equals("LAT")) {
                double lat2 = Math.toRadians(Double.parseDouble(station.getLAT()));
                double lon2 = Math.toRadians(Double.parseDouble(station.getLON()));
                double dist = Math.acos(Math.sin(lat1) * Math.sin(lat2) + Math.cos(lat1) * Math.cos(lat2) * (Math.cos(lon1 - lon2))) * 6371;
                if (!Double.isNaN(dist) && dist <= 50d)
                    stationsList.add(station);
            }
        }
        return stationsList;
    }


    private int parseUsaf(String usaf) {
        if (usaf.matches("^[A-Za-z]\\d+$")) {
            usaf = usaf.substring(1);
            return Integer.parseInt(usaf) + 'A' * 1000000;
        }
        return Integer.parseInt(usaf);
    }
}
