package com.progressoft.repository;

import com.progressoft.model.Station;
import com.progressoft.util.StationMapper;
import com.progressoft.util.database.connectionpool.ConnectionPool;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StationDAODB implements StationDAO {
    public static final String SELECT_ALL_STATITONS = "select * from stations";
    private static final String INSERT_RECORD = "INSERT INTO stations VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
    private final Connection connection;
    private final ConnectionPool pool;

    private final StationMapper stationMapper;

    public StationDAODB(ConnectionPool pool, StationMapper stationMapper) {
        this.pool = pool;
        connection = pool.getConnection();
        this.stationMapper = stationMapper;
    }


    @Override
    public void saveStation(Station station) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(INSERT_RECORD, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, null);
            preparedStatement.setString(2, station.getUSAF());
            preparedStatement.setString(3, station.getWBAN());
            preparedStatement.setString(4, station.getStationName());
            preparedStatement.setString(5, station.getCTRY());
            preparedStatement.setString(6, station.getST());
            preparedStatement.setString(7, station.getCALL());
            preparedStatement.setString(8, station.getLAT());
            preparedStatement.setString(9, station.getLON());
            preparedStatement.setString(10, station.getELEV());
            preparedStatement.setString(11, station.getBeginDate());
            preparedStatement.setString(12, station.getEndDate());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            pool.releaseConnection(connection);
        }
    }

    @Override
    public List<Station> getStations() {
        List<Station> stationList = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement(SELECT_ALL_STATITONS);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                stationList.add(stationMapper.convert(resultSet));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            pool.releaseConnection(connection);

        }
        return stationList;
    }

    @Override
    public List<Station> getStationByName(String name) {
        List<Station> stationList = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement(searchBy(name));
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                stationList.add(stationMapper.convert(resultSet));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            pool.releaseConnection(connection);
        }
        return stationList;
    }

    @Override
    public List<Station> getByCountryCode(String code) {
        List<Station> stationList = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement(findBy("CTRY"));
            statement.setString(1, code);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                stationList.add(stationMapper.convert(resultSet));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            pool.releaseConnection(connection);
        }
        return stationList;
    }

    @Override
    public List<Station> getByStationsIDRange(String minRange, String maxRange) {
        List<Station> stationList = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement(findBetween(minRange, maxRange));
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                stationList.add(stationMapper.convert(resultSet));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            pool.releaseConnection(connection);
        }
        return stationList;
    }

    @Override
    public List<Station> getByGeographicalLocation(String LAT, String LON) {
        List<Station> stationList = new ArrayList<>();

        try {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM stations WHERE acos(sin(RADIANS(?)) * sin(RADIANS(LAT)) + cos(RADIANS(?))" +
                    " * cos(RADIANS(LAT)) * cos(RADIANS(LON) - (RADIANS(?)))) * 6371 <= 50;");
            statement.setString(1, LAT);
            statement.setString(2, LAT);
            statement.setString(3, LON);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                stationList.add(stationMapper.convert(resultSet));
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            pool.releaseConnection(connection);
        }
        return stationList;
    }

    private String findBy(String where) {
        return "SELECT * FROM stations WHERE " + where + "  = ?";
    }

    private String searchBy(String like) {
        return "SELECT * from stations WHERE STATION_NAME LIKE'%" + like + "%'";
    }

    private String findBetween(String arg1, String arg2) {
        return "SELECT * from stations WHERE USAF BETWEEN " + arg1 + " AND " + arg2;
    }

}
