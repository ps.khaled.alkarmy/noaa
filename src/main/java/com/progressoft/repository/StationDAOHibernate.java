package com.progressoft.repository;

import com.progressoft.model.Station;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

public class StationDAOHibernate implements StationDAO {


    private final Session currentSession;


    public StationDAOHibernate() {
        currentSession = new Configuration()
                .configure("hibernate.cfg.xml").buildSessionFactory().openSession();

    }

    @Override
    public void saveStation(Station station) {
        currentSession.beginTransaction();
        currentSession.persist(station);
        currentSession.getTransaction().commit();
    }

    @Override
    public List<Station> getStations() {
        currentSession.beginTransaction();
        Query<Station> fromStations = currentSession.createQuery("from Station ", Station.class);
        return fromStations.list();
    }

    @Override
    public List<Station> getStationByName(String name) {
        Query<Station> stationQuery = currentSession.createQuery("from Station where StationName like :name ", Station.class).setParameter("name", "%" + name + "%");
        return stationQuery.list();
    }

    @Override
    public List<Station> getByCountryCode(String code) {
        Query<Station> stationQuery = currentSession.createQuery("from Station where CTRY like :name ", Station.class).setParameter("name", "%" + code + "%");
        return stationQuery.list();
    }

    @Override
    public List<Station> getByStationsIDRange(String minRange, String maxRange) {

        Query<Station> stationQuery = currentSession.createQuery("from Station where USAF >= :minRange and USAF <= :maxRange", Station.class)
                .setParameter("minRange", minRange)
                .setParameter("maxRange", maxRange);

        return stationQuery.list();
    }

    @Override
    public List<Station> getByGeographicalLocation(String LAT, String LON) {
        if (!isActiveTransaction()) {
            currentSession.beginTransaction();
        }

        Query<Station> stationQuery = currentSession.createNativeQuery("SELECT * FROM stations WHERE acos(sin(RADIANS(?)) * sin(RADIANS(LAT)) + cos(RADIANS(?)) * cos(RADIANS(LAT)) * cos(RADIANS(LON) - (RADIANS(?)))) * 6371 <= 50", Station.class)
                .setParameter(1, LAT)
                .setParameter(2, LAT)
                .setParameter(3, LON);
        return stationQuery.list();
    }

    private boolean isActiveTransaction() {
        return currentSession.getTransaction().isActive();
    }
}
